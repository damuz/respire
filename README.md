# RESPIRE
Respire é um projeto de desenvolvimento colaborativo de um ventilador mecânico emergencial de rápida manufatura. 

Nossa missão é produzir conhecimento e buscar soluções emergenciais no combate ao Covid-19, em particular pela falta de ventiladores mecânicos.

Três instituições de ensino do Distrito Federal juntaram esforços para desenvolver **solucões de hardware e software Open Source** e de baixo custo.

## Colaboradores: ##
Equipe constituida por engenheiros mecânicos, eletrônicos, mecatrônicos, médicos e estudantes de engenharia.
* Universidade de Brasilia
* Instituto Federal de Brasilia
* Uniceplac

### Objetivo: ###
Construção de um protótipo funcional open source e de baixo custo de um ventilador mecânico. 

**Nota**: Este projeto não objetiva a realização de ensaios clínicos. Não deve ser usado em animais ou humanos sem a prévia autorização de um comitê de ética em pesquisa.

### Facilidades: ###
Os Professores participantes do projeto tem experiência em:
* Projeto mecânico
* Projeto eletrônico
* Processamento de sinais

Os laboratórios disponíveis nas três instituições incluem:
* Simulação realistica (manequins e software para configurar resistência pulmonar, monitoramento de parâmetros vitais)
* Impressoras 3D 
* Confeccção de placas de circuito impresso e soldagem SMD 
* Plotter para corte a laser
* Tornos e fresadoras CNC para confecção de moldes 
* (Em fase de implantação): injeção de plástico para manufatura rápida

### Informações ###
* O ventilador consiste na automação de um reanimador manual AMBU.
* Prototipo concebido sob as especificações contidas no documento “Rapidly manufactured ventilator system specification” do Department of Health & Social Care do Reino Unido ([https://www.gov.uk/government/publications/coronavirus-covid-19-ventilator-supply-specification/])
* O ventilador permite configurar um volume corrente e obter um fluxo de saída regulado por pressão.
* O ventilador possui um sistema de monitoramento e alarmes que permita a equipe médica acompanhar o pico de pressão, pressão de platô, PEEP e frequência respiratória.

### Doações ###
* Nós ajude a patrocinar este projeto através da campanha de arrecadação [http://vaka.me/982620]
* Mostraremos com transparência a destinação do dinheiro arrecadado.
